#pragma once
#include <iostream>
#include <set>
#include <queue>
#include <chrono>
#include <mutex>
#include <condition_variable>

class MessagesSender 
{
	std::set <std::string> users;
	std::queue <std::string> messages;
	std::mutex userMtx;
	std::mutex mesgMtx;

public:
	MessagesSender() {};
	~MessagesSender() {};

	void LogReg();
	void readData();
	void writeData();
};
